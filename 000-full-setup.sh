echo "@@@@--------creating restore point -----------@@@"
yes | sudo pacman -Sy
yes | sudo pacman -S timeshift
sudo timeshift --create --comments "vanilla"
echo " @@@--!!!!!---please configure timeshift and please include the home folder in it -----!!!---@@@"
gksu timeshift-gtk &&
echo "@@@@-------installing additional virt-jaro softwares---------@@@@"
sh 003-install-additional-softwares.sh
echo "@@@@--------updating to the latest version---------@@@@"
sh 001-update-all.sh
echo "@@@---------installing libvirt---------@@@"
sh 002-install-libvirt.sh
echo "@@@---------copying libvirt settings---------@@@"
sh 0021-untarlibvirt.sh
echo "@@@----------installing nomachine---------@@@"
sh 004-install-nomachine.sh
echo "@@@------------copying i3 config-------------@@@"
sh 005-copy-config-i3.sh
echo "@@@----------replacing fstab-----------@@@"
sh 006-copy-fstab.sh
echo "@@@--------------restoring grub menu------------@@@"
sh 007-grub-modif.sh
echo "@@@---------creating VMs folders-----------@@@@"
sh 008-create-VMs-folders.sh
echo "@@@---------copying VMs HDs, be patient------------@@@"
sh 009-copy-VMs-HDs-from-source.sh
echo "@@@@@--------job done, enjoy Virt-jaro-----------@@@@"